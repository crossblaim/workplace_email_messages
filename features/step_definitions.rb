Before("@emailmessages") do
  @service = EmailMessages::Resource.new
  @client = @service.client
end

After("@emailmessages") do
  # shared cleanup logic
end
