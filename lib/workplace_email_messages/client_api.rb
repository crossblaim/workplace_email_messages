module EmailMessages
  # @api private
  module ClientApi

    include Seahorse::Model

    Empty = Shapes::StructureShape.new(name: 'Empty')
    PostEmail_messagesSet_as_processedRequest = Shapes::StructureShape.new(name: 'PostEmail_messagesSet_as_processedRequest')
    PostEmail_messagesSet_as_processedResponse = Shapes::StructureShape.new(name: 'PostEmail_messagesSet_as_processedResponse')
    PutEmail_messagesRequest = Shapes::StructureShape.new(name: 'PutEmail_messagesRequest')
    PutEmail_messagesResponse = Shapes::StructureShape.new(name: 'PutEmail_messagesResponse')
    boolean = Shapes::BooleanShape.new(name: 'boolean')
    double = Shapes::FloatShape.new(name: 'double')
    integer = Shapes::IntegerShape.new(name: 'integer')
    long = Shapes::IntegerShape.new(name: 'long')
    string = Shapes::StringShape.new(name: 'string')
    timestampIso8601 = Shapes::TimestampShape.new(name: 'timestampIso8601', timestampFormat: "iso8601")
    timestampUnix = Shapes::TimestampShape.new(name: 'timestampUnix', timestampFormat: "iso8601")

    Empty.struct_class = Types::Empty

    PostEmail_messagesSet_as_processedRequest.struct_class = Types::PostEmail_messagesSet_as_processedRequest

    PostEmail_messagesSet_as_processedResponse.add_member(:empty, Shapes::ShapeRef.new(shape: Empty, required: true, location_name: "Empty"))
    PostEmail_messagesSet_as_processedResponse.struct_class = Types::PostEmail_messagesSet_as_processedResponse
    PostEmail_messagesSet_as_processedResponse[:payload] = :empty
    PostEmail_messagesSet_as_processedResponse[:payload_member] = PostEmail_messagesSet_as_processedResponse.member(:empty)

    PutEmail_messagesRequest.struct_class = Types::PutEmail_messagesRequest

    PutEmail_messagesResponse.add_member(:empty, Shapes::ShapeRef.new(shape: Empty, required: true, location_name: "Empty"))
    PutEmail_messagesResponse.struct_class = Types::PutEmail_messagesResponse
    PutEmail_messagesResponse[:payload] = :empty
    PutEmail_messagesResponse[:payload_member] = PutEmail_messagesResponse.member(:empty)


    # @api private
    API = Seahorse::Model::Api.new.tap do |api|

      api.version = "2018-04-12T16:28:24Z"

      api.metadata = {
        "endpointPrefix" => "xwtvwgqp60",
        "protocol" => "api-gateway",
        "serviceFullName" => "testEmailTableUpdateApi",
      }

      api.add_operation(:post_email_messages_set_as_processed, Seahorse::Model::Operation.new.tap do |o|
        o.name = "PostEmail_messagesSet_as_processed"
        o.http_method = "POST"
        o.http_request_uri = "/development/email_messages/set_as_processed"
        o['authtype'] = "none"
        o.input = Shapes::ShapeRef.new(shape: PostEmail_messagesSet_as_processedRequest)
        o.output = Shapes::ShapeRef.new(shape: PostEmail_messagesSet_as_processedResponse)
      end)

      api.add_operation(:put_email_messages, Seahorse::Model::Operation.new.tap do |o|
        o.name = "PutEmail_messages"
        o.http_method = "PUT"
        o.http_request_uri = "/development/email_messages"
        o['authtype'] = "none"
        o.input = Shapes::ShapeRef.new(shape: PutEmail_messagesRequest)
        o.output = Shapes::ShapeRef.new(shape: PutEmail_messagesResponse)
      end)
    end

  end
end
