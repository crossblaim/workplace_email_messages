module EmailMessages
  module Types

    class Empty < Aws::EmptyStructure; end

    # @api private
    #
    class PostEmail_messagesSet_as_processedRequest < Aws::EmptyStructure; end

    # @!attribute [rw] empty
    #   @return [Types::Empty]
    #
    class PostEmail_messagesSet_as_processedResponse < Struct.new(
      :empty)
      include Aws::Structure
    end

    # @api private
    #
    class PutEmail_messagesRequest < Aws::EmptyStructure; end

    # @!attribute [rw] empty
    #   @return [Types::Empty]
    #
    class PutEmail_messagesResponse < Struct.new(
      :empty)
      include Aws::Structure
    end

  end
end
