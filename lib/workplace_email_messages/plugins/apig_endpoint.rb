module EmailMessages
  module Plugins

    class APIGEndpoint < Seahorse::Client::Plugin

      def after_initialize(client)
        if client.config.endpoint.nil?
          client.config.endpoint = 'https://xwtvwgqp60.execute-api.us-east-1.amazonaws.com'
        end
      end

    end
  end
end
