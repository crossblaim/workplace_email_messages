require 'aws-sdk-core'
require 'aws-sigv4'

require_relative 'workplace_email_messages/types'
require_relative 'workplace_email_messages/client_api'
require_relative 'workplace_email_messages/client'
require_relative 'workplace_email_messages/errors'
require_relative 'workplace_email_messages/resource'
require_relative 'workplace_email_messages/customizations'

# This module provides support for testEmailTableUpdateApi. This module is available in the
# `workplace_email_messages` gem.
#
# # Client
#
# The {Client} class provides one method for each API operation. Operation
# methods each accept a hash of request parameters and return a response
# structure.
#
# See {Client} for more information.
#
# # Errors
#
# Errors returned from testEmailTableUpdateApi all
# extend {Errors::ServiceError}.
#
#     begin
#       # do stuff
#     rescue EmailMessages::Errors::ServiceError
#       # rescues all service API errors
#     end
#
# See {Errors} for more information.
#
# @service
module EmailMessages

  GEM_VERSION = '1.0.0'

end
